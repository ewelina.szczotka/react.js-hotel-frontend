import React, { Component } from "react";
import "./App.css";
import { Route } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";

import Home from "./components/home";
import Rooms from "./components/rooms";
import Contact from "./components/contact";
import Login from "./components/login";
import Signup from "./components/signup";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/footer";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <BrowserRouter>
          <NavigationBar />
          <Route exact path="/home" component={Home} />
          <Route path="/rooms" component={Rooms} />
          <Route path="/contact" component={Contact} />
          <Route path="/login" component={Login} />
          <Route path="/signup" component={Signup} />
          <Footer />
        </BrowserRouter>
      </React.Fragment>
    );
  }
}

export default App;

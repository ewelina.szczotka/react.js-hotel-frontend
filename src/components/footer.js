import React, { Component } from "react";
import { Navbar } from "react-bootstrap";

class footer extends Component {
  render() {
    return (
      <Navbar>
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text className="text-center">
            &copy;The Hotel Contact: hotel@hotel.com
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default footer;

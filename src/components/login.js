import React, { Component } from "react";
import {
  Button,
  FormGroup,
  FormControl,
  FormLabel,
  Form
} from "react-bootstrap";
import "./login.css";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: null,
      password: null
    };
  }

  handleSubmit = event => {
    event.preventDefault();

    /* check e-mail in database
    check password in database
    if correct - login  */
  };

  render() {
    return (
      <div className="Login">
        <Form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <FormLabel>Email:</FormLabel>
            <FormControl
              autoFocus
              placeholder="Enter email"
              type="email"
              name="email"
              value={this.state.password}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <FormLabel>Password:</FormLabel>
            <FormControl
              value={this.state.password}
              placeholder="Password"
              type="password"
              name="password"
            />
          </FormGroup>
          <Button block variant="secondary" bsSize="large" type="submit">
            Login
          </Button>
        </Form>
      </div>
    );
  }
}

export default Login;

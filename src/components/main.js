import React from "react";
import { Switch, Route } from "react-router-dom";
import Landing from "./landingpage";
import Rooms from "./rooms";
import Contact from "./contact";
import Login from "./login";
import Signup from "./signup";

const Main = () => (
  <Switch>
    <Route exact path="/" component={Landing} />
    <Route exact path="/rooms" component={Rooms} />
    <Route exact path="/contact" component={Contact} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/signup" component={Signup} />
  </Switch>
);

export default Main;

import React, { Component } from "react";
import { Container, Row, Col, Card, Badge } from "react-bootstrap";
import "./rooms.css";

class Rooms extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <Container className="grid">
          <Row>
            <Col xs="12" sm="6" className="room-wrapper">
              <Card className="text-center" border="secondary">
                <Card.Img src="/images/single-room.jpg" variant="top" />
                <Card.Body>
                  <Card.Title>
                    Single Room{" "}
                    <Badge pill variant="secondary">
                      40$
                    </Badge>
                  </Card.Title>
                  <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col xs="12" sm="6" className="room-wrapper">
              <Card className="text-center" border="secondary">
                <Card.Img src="/images/double-room.jpg" variant="top" />
                <Card.Body>
                  <Card.Title>
                    Double Room{" "}
                    <Badge pill variant="secondary">
                      80$
                    </Badge>
                  </Card.Title>
                  <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" className="room-wrapper">
              <Card className="text-center" border="secondary">
                <Card.Img src="/images/apartment.jpg" variant="top" />
                <Card.Body>
                  <Card.Title>
                    Apartment{" "}
                    <Badge pill variant="secondary">
                      100$
                    </Badge>
                  </Card.Title>
                  <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col xs="12" sm="6" className="room-wrapper">
              <Card className="text-center" border="secondary">
                <Card.Img src="/images/studio.jpg" variant="top" />
                <Card.Body>
                  <Card.Title>
                    Studio{" "}
                    <Badge pill variant="secondary">
                      150$
                    </Badge>
                  </Card.Title>
                  <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Rooms;

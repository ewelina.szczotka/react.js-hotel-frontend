import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import "./signup.css";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      surname: null,
      email: null,
      password: null,
      errors: {
        name: "Name must be 3 characters long.",
        surname: "Surname must be 3 characters long.",
        email: "Email is not valid.",
        password: "Password must be 6 characters long."
      }
    };
  }

  handleUserInput = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let errors = this.state.errors;

    switch (name) {
      case "name":
        errors.name = value.length < 3 ? "Name must be 3 characters long." : "";
        break;
      case "surname":
        errors.surname =
          value.length < 3 ? "Surname must be 3 characters long." : "";
        break;
      case "email":
        errors.email = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
          ? ""
          : "Email is not valid.";
        break;
      case "password":
        errors.password =
          value.length < 6 ? "Password must be 6 characters long." : "";
        break;
      default:
        break;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (validateForm(this.state.errors)) {
      /* todo - server connection */
    } else {
      console.info("Invalid form");
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="Signup">
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="formGridName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              placeholder="Name"
              name="name"
              type="text"
              isValid={errors.name.length === 0}
              value={this.state.name}
              onChange={this.handleUserInput}
            />
            <Form.Text className="text-muted">
              {this.state.name ? errors.name : ""}
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formGridSurname">
            <Form.Label>Surname</Form.Label>
            <Form.Control
              placeholder="Surname"
              name="surname"
              type="text"
              isValid={errors.surname.length === 0}
              value={this.state.surname}
              onChange={this.handleUserInput}
            />
            <Form.Text className="text-muted">
              {this.state.surname ? errors.surname : ""}
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formGridEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              name="email"
              placeholder="Enter email"
              isValid={errors.email.length === 0}
              value={this.state.email}
              onChange={this.handleUserInput}
            />
            <Form.Text className="text-muted">
              {this.state.email ? errors.email : ""}
            </Form.Text>
          </Form.Group>
          <Form.Group controlId="formGridPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Enter password"
              isValid={errors.password.length === 0}
              value={this.state.password}
              onChange={this.handleUserInput}
            />
            <Form.Text className="text-muted">
              {this.state.password ? errors.password : ""}
            </Form.Text>
          </Form.Group>
          <Button block bsSize="large" type="submit" variant="secondary">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}

const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};

export default Signup;
